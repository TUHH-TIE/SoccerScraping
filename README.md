# Soccer Scraping

Simply install all requirements with python 3:

```
pip3 install -r requirements.txt
```

Then you can run scrapy, by default it will start 32 processes to scrape a lot
of pages in parallel. The results will end up in `results.sqlite` and you can
get an additional JSON output if you wish:

```
scrapy crawl entry -o results.json -L WARNING
```

Omit the `-L WARNING` if you want to see a lot of output indicating that the
tool is still running ;).
