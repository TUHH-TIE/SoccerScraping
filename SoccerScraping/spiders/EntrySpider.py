from datetime import datetime

import re

import scrapy


def clean_text(text):
    if text is None:
        return ''

    text = text.strip().strip('†').strip()
    if text == '-':
        return ''

    return text


def str_to_int(string):
    """
    Converts the given string from the website to an integer. However it also
    handles things like a `-` to indicate zeroes.
    """
    cleaned = clean_text(string)
    if not cleaned:
        return 0

    return int(cleaned)


def text_to_year(text):
    """
    Parses the text given as something like 93/94 to a year like 1993.
    """
    text = clean_text(text)
    current_year = datetime.now().year
    year = '20' + text[:2]
    if int(year) > current_year:
        year = '19' + text[:2]

    return int(year)


def get_text(selector):
    """
    Retrieves the text of the selector whitespace cleaned.
    """
    return clean_text(selector.xpath('text()').extract_first())


def none_if_empty(text):
    return text if text != '' else None


class EntrySpider(scrapy.Spider):
    name = 'entry'

    def start_requests(self):
        urls = [
            'http://www.transfermarkt.de/serie-a/startseite/wettbewerb/IT1',
            'http://www.transfermarkt.de/premier-league/startseite/wettbewerb/GB1',
            'http://www.transfermarkt.de/primera-division/startseite/wettbewerb/ES1',
            'http://www.transfermarkt.de/ligue-1/startseite/wettbewerb/FR1',
            'http://www.transfermarkt.de/1-bundesliga/startseite/wettbewerb/L1',
            'http://www.transfermarkt.de/liga-nos/startseite/wettbewerb/PO1',
        ]

        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse_league)

    def parse_league(self, response):
        part_url = response.url.split('/')

        # Get ranking for all years
        for year in range(2005, 2016 + 1):
            url = 'http://www.transfermarkt.de/thisisarbitrary/tabelle/wettbewerb/{league}/saison_id/{year}'
            url = url.format(league=part_url[-1], year=str(year))
            yield scrapy.Request(url, self.parse_league_table)

    def parse_league_table(self, response):

        id = response.url.split('/')[-3]
        year = response.url[-4:]

        for tr_elem in response.xpath("//th[text()='#']/../../../tbody/tr"):
            elems = list(tr_elem.xpath('td'))

            if len(elems) < 10:
                continue

            club_id = elems[2].xpath('a/@id').extract_first()
            yield {
                'type': 'ranking',
                'club_id': int(club_id),
                'league': id,
                'year': int(year),
                'rank': str_to_int(get_text(elems[0])),
                'num_games': str_to_int(get_text(elems[3].xpath('a'))),
                'num_win': str_to_int(get_text(elems[4].xpath('a'))),
                'num_draw': str_to_int(get_text(elems[5].xpath('a'))),
                'num_lost': str_to_int(get_text(elems[6].xpath('a'))),
                'goal_ratio': get_text(elems[7]),
                'points': str_to_int(get_text(elems[9])),
            }

            club_url = 'http://www.transfermarkt.de/thisisarbitrary/spielplan/verein/{id}'
            yield scrapy.Request(club_url.format(id=club_id), self.parse_club)

            kader_url = 'http://www.transfermarkt.de/thisisarbitrary/kader/verein/{id}/saison_id/{year}/plus/1'
            yield scrapy.Request(kader_url.format(id=club_id, year=year),
                                 self.parse_kader)

    def parse_club(self, response):
        club_id = response.url.split('/')[-1]

        yield {
            'type': 'club',
            'club_id': int(club_id),
            'nationality': response.xpath("//img[@class='flaggenrahmen vm']/@alt").extract_first(),
            'club_name': get_text(response.xpath("//h1/b")),
        }

        coaches_url = 'http://www.transfermarkt.de/thisisarbitrary/mitarbeiterhistorie/verein/{id}'
        yield scrapy.Request(coaches_url.format(id=club_id),
                             self.parse_club_coaches)

        intl_ranking_url = 'http://www.transfermarkt.de/thisisarbitrary/pokalhistorie/verein/{id}'
        yield scrapy.Request(intl_ranking_url.format(id=club_id),
                             self.parse_intl_ranking)

    def parse_kader(self, response):
        """
        E.g.
        - http://www.transfermarkt.de/thisisarbitrary/kader/verein/23/saison_id/2013/plus/1
        - http://www.transfermarkt.de/thisisarbitrary/kader/verein/2448/saison_id/2016/plus/1
        """
        year = response.url.split('/')[-3]
        club_id = response.url.split('/')[-5]
        for player_td in response.xpath("//tbody/tr"):
            elems = list(player_td.xpath('td'))
            if len(elems) < 10:
                continue

            info_table = list(elems[1].xpath("table//tr"))
            player_id = info_table[0].xpath('*[2]/*[1]/span/a/@href'
                                            ).extract_first().split('/')[-1]

            yield {
                'type': 'playerposition',
                'player_id': str_to_int(player_id),
                'club_id': str_to_int(club_id),
                'year': int(year),
                'position': get_text(info_table[1].xpath('td')),
                'value': get_text(elems[-1]),
            }

            # Do a URL join here to avoid an unnneeded redirect. This is the
            # only case where they do a redirect if the arbitrary part of the
            # URL isn't correct.
            player_url = response.urljoin(info_table[0].xpath(
                '*[2]/*[1]/span/a/@href').extract_first())
            yield scrapy.Request(player_url, self.parse_player)

            player_perf_url = "http://www.transfermarkt.de/thisisarbitrary/detaillierteleistungsdaten/spieler/{id}/plus/1"
            yield scrapy.Request(player_perf_url.format(id=player_id),
                                 self.parse_player_perf)

            player_transfer_url = "http://www.transfermarkt.de/thisisarbitrary/transfers/spieler/{id}"
            yield scrapy.Request(player_transfer_url.format(id=player_id),
                                 self.parse_player_transfers)

    def parse_player(self, response):
        """
        Parses player info, like this page:

        http://www.transfermarkt.de/manuel-neuer/profil/spieler/17259
        """
        national_matches = 0
        national_first_games = []
        for match in response.xpath(
                "//div[text()='Länderspielkarriere']/../table/tbody/tr"):
            elems = list(match.xpath('td'))
            if len(elems) < 6:
                continue

            team = get_text(elems[2].xpath('a'))
            if re.match('.* U[0-9]+', team) is not None:
                continue  # This is a youth team, U20 e.g.

            national_matches += str_to_int(get_text(elems[4].xpath('a')))
            national_first_games.append(get_text(elems[3].xpath('a')))


        sponsor = response.xpath(
            "//th[starts-with(text(), 'Ausrüster')]/../td[1]/text()"
        ).extract()
        if len(sponsor) > 0:
            sponsor = sponsor[-1].strip()
        else:
            sponsor = None

        try:
            national_first_game = min(
                tuple(reversed(debut.split('.')))
                for debut in national_first_games
                if debut)
            national_first_game = '.'.join(reversed(national_first_game))
        except ValueError:  # No national game
            national_first_game = None

        yield {
            'type': 'player',
            'player_id': response.url.split('/')[-1],
            'first_name': get_text(response.xpath('//h1')),
            'last_name': get_text(response.xpath('//h1/b')),
            'birth': get_text(response.xpath(
                "//th[text()='Geburtsdatum:']/../td/a"
            )),
            'nationality': response.xpath(
                "//span[text()='Nationalität:']/../span/img"
                "[@class='flaggenrahmen']/@alt").extract_first(),
            'sponsor': sponsor,
            'national_matches': national_matches,
            'national_first_game': national_first_game,
            'agent': none_if_empty(get_text(response.xpath(
                "//span[text()='Berater:']/../span[2]/a"
            ))),
        }

    def parse_player_perf(self, response):
        """
        Scrapes performance info for a given player. Examples:

        http://www.transfermarkt.de/massimiliano-allegri/detaillierteleistungsdaten/spieler/163501/plus/1
        http://www.transfermarkt.de/roman-weidenfeller/detaillierteleistungsdaten/spieler/26/plus/1

        Note that the tables differ (the latter is a goalkeeper!).
        """
        player_id = int(response.url.split('/')[-3])

        for row in response.xpath(
                "//div[text()='Nationale Ligen']/..//table/tbody/tr"):
            elems = list(row.xpath('td'))

            if len(elems) < 15:
                continue

            record = {
                'player_id': player_id,
                'year': text_to_year(get_text(elems[0])),
                'league': elems[2].xpath(
                    'a/@href').extract_first().split('/')[-1],
                'club_id': int(elems[3].xpath("a/@id").extract_first()),
                'num_games': str_to_int(get_text(elems[4].xpath('a'))),
                'num_goals': str_to_int(get_text(elems[5]))
            }

            if len(elems) == 15:
                record.update({
                    'type': 'goalkeeperperformance',
                    'num_owngoals': str_to_int(get_text(elems[6])),
                    'num_subins': str_to_int(get_text(elems[7])),
                    'num_subouts': str_to_int(get_text(elems[8])),
                    'num_yellows': str_to_int(get_text(elems[9])),
                    'num_yellowreds': str_to_int(get_text(elems[10])),
                    'num_reds': str_to_int(get_text(elems[11])),
                    'num_goals_conceived': str_to_int(get_text(elems[12])),
                    'num_games_wo_goals_conceived': str_to_int(
                        get_text(elems[13])),
                    'minutes_played': str_to_int(
                        get_text(elems[14])[:-1].replace('.', '')),
                })
                yield record
            elif len(elems) == 16:
                record.update({
                    'type': 'playerperformance',
                    'num_assists': str_to_int(get_text(elems[6])),
                    'num_owngoals': str_to_int(get_text(elems[7])),
                    'num_subins': str_to_int(get_text(elems[8])),
                    'num_subouts': str_to_int(get_text(elems[9])),
                    'num_yellows': str_to_int(get_text(elems[10])),
                    'num_yellowreds': str_to_int(get_text(elems[11])),
                    'num_reds': str_to_int(get_text(elems[12])),
                    'num_penalty_goals': str_to_int(get_text(elems[13])),
                    'minutes_played': str_to_int(
                        get_text(elems[15])[:-1].replace('.', '')),
                })
                yield record

    def parse_club_coaches(self, response):
        """
        Parses the club coach history:

        http://www.transfermarkt.de/juventus-turin/mitarbeiterhistorie/verein/506
        """
        club_id = response.url.split('/')[-1]
        for coach_row in response.xpath("//tbody/tr"):
            elems = list(coach_row.xpath('td'))
            if len(elems) < 7:
                continue

            coach_link = response.urljoin(elems[0].xpath(
                "table//tr[1]/td[2]/a/@href").extract_first())
            coach_id = coach_link.split('/')[-1]

            yield {
                'type': 'coachclub',
                'coach_id': coach_id,
                'club_id': club_id,
                'start_coaching_date': get_text(elems[2]),
                'end_coaching_date': get_text(elems[3]),
            }

            yield scrapy.Request(coach_link, self.parse_coach)

    def parse_coach(self, response):
        """
        Parses info about a specific coach, e.g.:

        http://www.transfermarkt.de/massimiliano-allegri/profil/trainer/7671
        """
        name = get_text(response.xpath("//h1"))
        split_name = name.split(' ')

        player_id = response.xpath(
            "//a[@class='spielprofil_tooltip']/@href"
        ).extract_first()
        if player_id is not None:
            player_id = player_id.split('/')[-1]

        yield {
            'type': 'coach',
            'coach_id': response.url.split('/')[-1],
            'first_name': ' '.join(split_name[:-1]),
            'last_name': split_name[-1],
            'player_id': player_id,
            'nationality': get_text(response.xpath(
                "//span[@itemprop='nationality']")),
            'birth': get_text(response.xpath(
                "//table[@class='auflistung']"
                "//th[text()='Geburtsdatum:']/../td")),
        }

    def parse_player_transfers(self, response):
        """
        Parses a page like:

        http://www.transfermarkt.de/thisisarbitrary/transfers/spieler/163501
        """
        for transfer_link in response.xpath(
                "//img[@src='/images/pfeil_rechts_blau_k.png']/../@href"
        ).extract():
            yield scrapy.Request(response.urljoin(transfer_link),
                                 self.parse_player_transfer)

    def parse_player_transfer(self, response):
        """
        Parses a page like:

        http://www.transfermarkt.de/massimiliano-allegri/transfers/spieler/163501/transfer_id/489474

        Note, that sometimes, some fields seem to be not existent:

        http://www.transfermarkt.de/massimiliano-allegri/transfers/spieler/163501/transfer_id/489467
        """

        yield {
            'type': 'transfer',
            'transfer_id': response.url.split('/')[-1],
            'player_id': response.url.split('/')[-3],
            'from_club_id': str_to_int(response.xpath(
                "//div[text()[contains(., 'Transferdetails')]]/.."
                "//td[1]/a[2]/@id").extract_first()),
            'to_club_id': str_to_int(response.xpath(
                "//div[text()[contains(., 'Transferdetails')]]/.."
                "//td[3]/a[2]/@id").extract_first()),
            'date': get_text(response.xpath(
                "//b[text()='Transferzeitpunkt']/../a"
            )),
            'market_value': get_text(response.xpath(
                "//b[text()='Marktwert zum Zeitpunkt des Wechsels']/.."
            )),
            'payment': get_text(response.xpath("//p[@class='hauptfact']")),

            # League info
            'league_from': clean_text(response.xpath(
                "//td[text()='Wettbewerb']/../td[1]/a/@href"
            ).extract_first()).split('/')[-1],
            'league_to': clean_text(response.xpath(
                "//td[text()='Wettbewerb']/../td[3]/a/@href"
            ).extract_first()).split('/')[-1],
            'league_from_kind': get_text(response.xpath(
                "//td[text()='Liga-Art']/../td[1]")),
            'league_to_kind': get_text(response.xpath(
                "//td[text()='Liga-Art']/../td[3]")),

            # Manager info
            'from_manager': str_to_int(clean_text(response.xpath(
                "//td[text()='Manager']/../td[1]/a/@href"
            ).extract_first()).split('/')[-1]),
            'to_manager': str_to_int(clean_text(response.xpath(
                "//td[text()='Manager']/../td[3]/a/@href"
            ).extract_first()).split('/')[-1]),
            'from_manager_name': clean_text(response.xpath(
                "//td[text()='Manager']/../td[1]/a/@title"
            ).extract_first()),
            'to_manager_name': clean_text(response.xpath(
                "//td[text()='Manager']/../td[3]/a/@title"
            ).extract_first()),

            # Trainer info
            'from_trainer': str_to_int(clean_text(response.xpath(
                "//td[text()='Trainer']/../td[1]/a/@href"
            ).extract_first()).split('/')[-1]),
            'to_trainer': str_to_int(clean_text(response.xpath(
                "//td[text()='Trainer']/../td[3]/a/@href"
            ).extract_first()).split('/')[-1]),
            'from_trainer_name': clean_text(response.xpath(
                "//td[text()='Trainer']/../td[1]/a/@title"
            ).extract_first()),
            'to_trainer_name': clean_text(response.xpath(
                "//td[text()='Trainer']/../td[3]/a/@title"
            ).extract_first()),
        }

        yield scrapy.Request(response.urljoin(response.xpath(
                "//td[text()='Trainer']/../td[1]/a/@href"
            ).extract_first()), self.parse_coach)

        yield scrapy.Request(response.urljoin(response.xpath(
                "//td[text()='Trainer']/../td[3]/a/@href"
            ).extract_first()), self.parse_coach)

    def parse_intl_ranking(self, response):
        """
        Parses info from a page like:

        http://www.transfermarkt.de/thisisarbitrary/pokalhistorie/verein/27
        """
        club_id = response.url.split('/')[-1]
        for row in response.xpath("//tr"):
            elems = list(row.xpath('td'))
            if len(elems) == 1:
                competition = get_text(elems[0].xpath('a'))
            if len(elems) < 5:
                continue

            saison_comp_url = elems[0].xpath(
                'a/@href').extract_first().split('/')

            yield {
                'type': 'clubranking',
                'club_id': club_id,
                'year': int(saison_comp_url[-1]),
                'competition': competition,
                'result': get_text(elems[1]),
            }
