# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from scrapy.exceptions import DropItem
from sqlalchemy import create_engine
from sqlalchemy.orm.session import sessionmaker

from .items import (Base, Player, PlayerPosition, PlayerPerformance,
                    GoalkeeperPerformance, Ranking, Club, ClubRanking,
                    CoachClub, Coach, Transfer)


DbItemConstructor = {
    klass.__tablename__: klass
    for klass in (Player, PlayerPosition, PlayerPerformance,
                  GoalkeeperPerformance, Ranking, Club, ClubRanking,
                  CoachClub, Coach, Transfer)}


class SoccerscrapingPipeline(object):

    def open_spider(self, spider):
        self.db_engine = create_engine('sqlite:///results.sqlite')
        # Create DB
        Base.metadata.create_all(self.db_engine)
        self.sessionmaker = sessionmaker(bind=self.db_engine)
        self.session = self.sessionmaker()

    def close_spider(self, spider):
        self.session.close()

    def process_item(self, item, spider):
        if 'type' not in item or item['type'] not in DbItemConstructor:
            raise DropItem

        db_item = DbItemConstructor[item['type']]()

        for key, value in item.items():
            setattr(db_item, key, value)

        self.session.add(db_item)
        self.session.commit()

        return item
