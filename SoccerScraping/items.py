# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative.api import declarative_base


Base = declarative_base()


class Player(Base):
    __tablename__ = 'player'

    player_id = Column(Integer, primary_key=True, nullable=False)
    first_name = Column(String, nullable=False)
    last_name = Column(String, nullable=False)
    birth = Column(String, nullable=False)
    nationality = Column(String, nullable=True)
    sponsor = Column(String, nullable=True)
    national_matches = Column(Integer, nullable=False)
    national_first_game = Column(String, nullable=True)
    agent = Column(String, nullable=True)


class PlayerPosition(Base):
    __tablename__ = 'playerposition'

    player_position_id = Column(Integer, primary_key=True,
                                autoincrement=True, nullable=False)
    player_id = Column(Integer, nullable=False)
    club_id = Column(Integer, nullable=False)
    year = Column(Integer, nullable=False)
    position = Column(String, nullable=False)
    value = Column(String, nullable=False)


class AllPlayersPerfMixin(object):
    """
    This contains columns that are shared between players and goalkeepers. This
    is not an own table! It's just used to compose PlayerPerformance and
    GoalkeeperPerformance.
    """
    player_id = Column(Integer, nullable=False)
    year = Column(Integer, nullable=False)
    league = Column(String, nullable=False)
    club_id = Column(Integer, nullable=False)
    num_games = Column(Integer, nullable=False)
    num_goals = Column(Integer, nullable=False)
    num_owngoals = Column(Integer, nullable=False)
    num_subins = Column(Integer, nullable=False)
    num_subouts = Column(Integer, nullable=False)
    num_yellows = Column(Integer, nullable=False)
    num_yellowreds = Column(Integer, nullable=False)
    num_reds = Column(Integer, nullable=False)
    minutes_played = Column(Integer, nullable=False)


class PlayerPerformance(AllPlayersPerfMixin, Base):
    __tablename__ = 'playerperformance'

    player_perf_id = Column(Integer, primary_key=True,
                            autoincrement=True, nullable=False)
    num_assists = Column(Integer, nullable=False)
    num_penalty_goals = Column(Integer, nullable=False)


class GoalkeeperPerformance(AllPlayersPerfMixin, Base):
    __tablename__ = 'goalkeeperperformance'

    goalkeeper_perf_id = Column(Integer, primary_key=True,
                                autoincrement=True, nullable=False)
    num_goals_conceived = Column(Integer, nullable=False)
    num_games_wo_goals_conceived = Column(Integer, nullable=False)



class Ranking(Base):
    __tablename__ = 'ranking'

    ranking_id = Column(Integer, primary_key=True,
                        autoincrement=True, nullable=False)
    club_id = Column(Integer, nullable=False)
    league = Column(String, nullable=False)
    year = Column(Integer, nullable=False)
    rank = Column(Integer, nullable=False)
    num_games = Column(Integer, nullable=False)
    num_win = Column(Integer, nullable=False)
    num_draw = Column(Integer, nullable=False)
    num_lost = Column(Integer, nullable=False)
    goal_ratio = Column(String, nullable=False)
    points = Column(Integer, nullable=False)


class Club(Base):
    __tablename__ = 'club'

    club_id = Column(Integer, primary_key=True, nullable=False)
    nationality = Column(String)
    club_name = Column(String, nullable=False)


class ClubRanking(Base):
    __tablename__ = 'clubranking'

    clubranking_id = Column(Integer, primary_key=True,
                            autoincrement=True, nullable=False)
    club_id = Column(Integer, nullable=False)
    year = Column(Integer, nullable=False)
    competition = Column(String, nullable=False)
    result = Column(String, nullable=False)


class CoachClub(Base):
    __tablename__ = 'coachclub'

    coachclub_id = Column(Integer, primary_key=True,
                          autoincrement=True, nullable=False)
    club_id = Column(Integer, nullable=False)
    coach_id = Column(Integer, nullable=False)
    start_coaching_date = Column(String, nullable=False)
    end_coaching_date = Column(String, nullable=False)


class Coach(Base):
    __tablename__ = 'coach'

    coach_id = Column(Integer, nullable=False, primary_key=True)
    first_name = Column(String, nullable=False)
    last_name = Column(String, nullable=False)
    player_id = Column(Integer, nullable=True)
    nationality = Column(String, nullable=False)
    birth = Column(String, nullable=False)


class Transfer(Base):
    __tablename__ = 'transfer'

    transfer_id = Column(Integer, primary_key=True, nullable=False)
    player_id = Column(Integer, nullable=False)

    from_club_id = Column(Integer, nullable=False)
    to_club_id = Column(Integer, nullable=False)

    date = Column(String, nullable=False)
    market_value = Column(String, nullable=False)
    payment = Column(String, nullable=False)

    league_from = Column(String, nullable=False)
    league_to = Column(String, nullable=False)
    league_from_kind = Column(String, nullable=False)
    league_to_kind= Column(String, nullable=False)

    from_manager = Column(Integer, nullable=False)
    to_manager = Column(Integer, nullable=False)
    from_manager_name = Column(String, nullable=False)
    to_manager_name = Column(String, nullable=False)

    from_trainer = Column(Integer, nullable=False)
    to_trainer = Column(Integer, nullable=False)
    from_trainer_name = Column(String, nullable=False)
    to_trainer_name = Column(String, nullable=False)
